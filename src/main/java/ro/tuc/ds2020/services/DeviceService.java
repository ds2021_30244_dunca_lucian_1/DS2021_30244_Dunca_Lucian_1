package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.dtos.PersonDetailsDTO;
import ro.tuc.ds2020.dtos.builders.DeviceBuilder;
import ro.tuc.ds2020.dtos.builders.PersonBuilder;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.entities.Sensor;
import ro.tuc.ds2020.entities.SmartDevice;
import ro.tuc.ds2020.repositories.DeviceRepository;
import ro.tuc.ds2020.repositories.PersonRepository;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class DeviceService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeviceService.class);
    private final DeviceRepository deviceRepository;

    @Autowired
    public DeviceService(DeviceRepository deviceRepository) {
        this.deviceRepository = deviceRepository;
    }



    public List<DeviceDTO> findDevices() {
        List<SmartDevice> deviceList = deviceRepository.findAll();
        return deviceList.stream()
                .map(DeviceBuilder::toDeviceDTO)
                .collect(Collectors.toList());
    }

    public DeviceDTO findDeviceById(Long id) {
        Optional<SmartDevice> prosumerOptional = deviceRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Person with id {} was not found in db", id);
            throw new ResourceNotFoundException(SmartDevice.class.getSimpleName() + " with id: " + id);
        }
        return DeviceBuilder.toDeviceDTO(prosumerOptional.get());
    }

    public Long insert(DeviceDTO personDTO, Long userId) {
        SmartDevice device = DeviceBuilder.toEntity(personDTO);
        device.setSensor(new Sensor(userId));
        device = deviceRepository.save(device);
        LOGGER.debug("Person with id {} was inserted in db", device.getId());
        return device.getId();
    }

    public DeviceDTO update(DeviceDTO newEmployee,  Long  id, Long sensorId) {
        try {
            DeviceDTO person = findDeviceById(id);

            SmartDevice tempPerson = DeviceBuilder.toEntity(newEmployee);

            tempPerson.setId(id);
            tempPerson.setSensor(new Sensor(sensorId));

            deviceRepository.save(tempPerson);
            return person;
        }catch(Exception e){
            SmartDevice tempPerson = DeviceBuilder.toEntity(newEmployee);

            tempPerson.setId(id);
            tempPerson.setSensor(new Sensor(sensorId));
            deviceRepository.save(tempPerson);
            return newEmployee;
        }

    }

    public void delete(Long id){
        deviceRepository.deleteById(id);

    }

    public List<DeviceDTO> checkDevice(Long personId ) {
        List<SmartDevice> personList = deviceRepository.findByPersonId(personId);
        return personList.stream()
                .map(DeviceBuilder::toDeviceDTO)
                .collect(Collectors.toList());

    }
}
