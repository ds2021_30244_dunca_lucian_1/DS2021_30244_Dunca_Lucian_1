package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.dtos.PersonDetailsDTO;
import ro.tuc.ds2020.dtos.SensorDTO;
import ro.tuc.ds2020.dtos.builders.DeviceBuilder;
import ro.tuc.ds2020.dtos.builders.PersonBuilder;
import ro.tuc.ds2020.dtos.builders.SensorBuilder;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.entities.Sensor;
import ro.tuc.ds2020.entities.SmartDevice;
import ro.tuc.ds2020.repositories.DeviceRepository;
import ro.tuc.ds2020.repositories.PersonRepository;
import ro.tuc.ds2020.repositories.SensorRepository;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class SensorService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SensorService.class);
    private final SensorRepository sensorRepository;

    @Autowired
    public SensorService(SensorRepository sensorRepository) {
        this.sensorRepository = sensorRepository;
    }



    public List<SensorDTO> findDevices() {
        List<Sensor> deviceList = sensorRepository.findAll();
        return deviceList.stream()
                .map(SensorBuilder::toSensorDTO)
                .collect(Collectors.toList());
    }

    public SensorDTO findDeviceById(Long id) {
        Optional<Sensor> prosumerOptional = sensorRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Person with id {} was not found in db", id);
            throw new ResourceNotFoundException(Sensor.class.getSimpleName() + " with id: " + id);
        }
        return SensorBuilder.toSensorDTO(prosumerOptional.get());
    }

    public Long insert(SensorDTO sensorDTO) {
        Sensor sensor = SensorBuilder.toEntity(sensorDTO);
        //device.setSensor(new Sensor(2L,"descr",44));
        sensor = sensorRepository.save(sensor);
        LOGGER.debug("Person with id {} was inserted in db", sensor.getId());
        return sensor.getId();
    }

    public SensorDTO update(SensorDTO newEmployee,  Long  id) {
        try {
            SensorDTO person = findDeviceById(id);

            Sensor tempPerson = SensorBuilder.toEntity(newEmployee);

            tempPerson.setId(id);
            sensorRepository.save(tempPerson);
            return person;
        }catch(Exception e){
            Sensor tempPerson = SensorBuilder.toEntity(newEmployee);

            tempPerson.setId(id);
            sensorRepository.save(tempPerson);
            return newEmployee;
        }

    }

    public void delete(Long id){
        sensorRepository.deleteById(id);

    }
}
