package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.SensorDataDTO;
import ro.tuc.ds2020.dtos.builders.SensorDataBuilder;
import ro.tuc.ds2020.entities.Sensor;
import ro.tuc.ds2020.entities.SensorData;
import ro.tuc.ds2020.entities.SmartDevice;
import ro.tuc.ds2020.repositories.SensorDataRepository;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class SensorDataService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeviceService.class);
    private final SensorDataRepository sensorDataRepository;

    @Autowired
    public SensorDataService(SensorDataRepository sensorDataRepository) {
        this.sensorDataRepository = sensorDataRepository;
    }



    public List<SensorDataDTO> findDevices() {
        List<SensorData> deviceList = sensorDataRepository.findAll();

        return deviceList.stream()
                .map(SensorDataBuilder::toSensorDTO)
                .collect(Collectors.toList());
    }

    public SensorDataDTO findDeviceById(Long id) {
        Optional<SensorData> prosumerOptional = sensorDataRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Person with id {} was not found in db", id);
            throw new ResourceNotFoundException(SmartDevice.class.getSimpleName() + " with id: " + id);
        }
        return SensorDataBuilder.toSensorDTO(prosumerOptional.get());
    }

    public Long insert2(SensorDataDTO personDTO) {
        SensorData device = SensorDataBuilder.toEntity(personDTO);
        device = sensorDataRepository.save(device);
        LOGGER.debug("Person with id {} was inserted in db", device.getId());
        return device.getId();
    }

    public Long insert(SensorDataDTO personDTO,Long sensorId) {
        SensorData device = SensorDataBuilder.toEntity(personDTO);
        device.setSensor(new Sensor(sensorId));
        device = sensorDataRepository.save(device);
        LOGGER.debug("Person with id {} was inserted in db", device.getId());
        return device.getId();
    }

    public void delete(Long id){
        sensorDataRepository.deleteById(id);

    }
}
