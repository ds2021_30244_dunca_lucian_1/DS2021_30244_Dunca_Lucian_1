package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.dtos.PersonDetailsDTO;
import ro.tuc.ds2020.dtos.builders.PersonBuilder;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.entities.SmartDevice;
import ro.tuc.ds2020.repositories.PersonRepository;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class PersonService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PersonService.class);
    private final PersonRepository personRepository;

    @Autowired
    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }


    public List<PersonDTO> findPersons() {
        List<Person> personList = personRepository.findAll();
        return personList.stream()
                .map(PersonBuilder::toPersonDTO)
                .collect(Collectors.toList());
    }

    public PersonDetailsDTO findPersonById(Long id) {
        Optional<Person> prosumerOptional = personRepository.findById(id);
        //System.out.println("merge: " +(prosumerOptional.get()).getDevice().get(0).getDescription());
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Person with id {} was not found in db", id);
            throw new ResourceNotFoundException(Person.class.getSimpleName() + " with id: " + id);
        }
        return PersonBuilder.toPersonDetailsDTO(prosumerOptional.get());
    }

    public Long insert(PersonDetailsDTO personDTO) {
        Person person = PersonBuilder.toEntity(personDTO);
        //person.addDevice(new SmartDevice(deviceId));
        person = personRepository.save(person);
        LOGGER.debug("Person with id {} was inserted in db", person.getId());
        return person.getId();
    }

    public PersonDetailsDTO update(PersonDetailsDTO newEmployee, Long id) {
        try {
            PersonDetailsDTO person = findPersonById(id);

            Person tempPerson = PersonBuilder.toEntity(newEmployee);

            tempPerson.setId(id);
            tempPerson.setDevice(person.getDevice());
            personRepository.save(tempPerson);
            return person;
        } catch (Exception e) {
            Person tempPerson = PersonBuilder.toEntity(newEmployee);

            tempPerson.setId(id);
            personRepository.save(tempPerson);
            return newEmployee;
        }
    }

    public void delete(Long id){
        personRepository.deleteById(id);

    }
    public void addDevice( Long idPerson, Long idDevice) {
        PersonDetailsDTO dto = findPersonById(idPerson);
        Person tempPerson = PersonBuilder.toEntity(dto);

        tempPerson.addDevice(new SmartDevice(idDevice));
        tempPerson.setId(idPerson);
        personRepository.save(tempPerson);

    }
    public Person checkPerson(PersonDetailsDTO person) {
        List<Person> personList = personRepository.findByName(person.getName());
        if(personList.size() == 0)
            return new Person();

        System.out.println(personList.get(0).getPassword());
        System.out.println(person.getPassword());
        if(personList.get(0).getPassword().equals(person.getPassword()))
            return personList.get(0);
            else return new Person();

    }
}
