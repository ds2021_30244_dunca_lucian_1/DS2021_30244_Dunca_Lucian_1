package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.entities.SmartDevice;

import java.util.List;


public interface DeviceRepository extends JpaRepository<SmartDevice, Long> {

    List<SmartDevice> findByPersonId(Long id);

}
