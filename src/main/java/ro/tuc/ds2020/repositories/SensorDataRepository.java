package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.tuc.ds2020.entities.SensorData;
import ro.tuc.ds2020.entities.SmartDevice;



public interface SensorDataRepository extends JpaRepository<SensorData, Long> {


}


