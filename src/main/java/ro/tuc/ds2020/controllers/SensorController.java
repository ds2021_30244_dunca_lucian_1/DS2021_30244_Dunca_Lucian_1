package ro.tuc.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.SensorDTO;
import ro.tuc.ds2020.services.DeviceService;
import ro.tuc.ds2020.services.SensorService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/sensor")
public class SensorController {

    private final SensorService sensorService;

    @Autowired
    public SensorController(SensorService sensorService) {
        this.sensorService = sensorService;
    }

    @GetMapping()
    public ResponseEntity<List<SensorDTO>> getDevices() {
        List<SensorDTO> dtos = sensorService.findDevices();
        for (SensorDTO dto : dtos) {
            Link personLink = linkTo(methodOn(DeviceController.class)
                    .getDevice(dto.getId())).withRel("personDetails");
            dto.add(personLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<Long> insertProsumer(@Valid @RequestBody SensorDTO sensorDTO) {
        Long deviceId = sensorService.insert(sensorDTO);
        return new ResponseEntity<>(deviceId, HttpStatus.CREATED);

    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<SensorDTO> getDevice(@PathVariable("id") Long sensorId) {
        SensorDTO dto = sensorService.findDeviceById(sensorId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }


    @PutMapping(value = "/updateSensor/{id}")
    SensorDTO replaceEmployee(@RequestBody SensorDTO newEmployee, @PathVariable("id") Long  id) {
        return sensorService.update(newEmployee, id);
    }


    @DeleteMapping(value = "/deleteSensor/{id}")
    public ResponseEntity<Long> deleteById(@PathVariable("id") Long  id){
        sensorService.delete(id);
        return new ResponseEntity<>(id, HttpStatus.CREATED);

    }

}

