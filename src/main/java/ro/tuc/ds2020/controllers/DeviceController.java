package ro.tuc.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.entities.SmartDevice;
import ro.tuc.ds2020.services.DeviceService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/device")
public class DeviceController {

    private final DeviceService deviceService;

    @Autowired
    public DeviceController(DeviceService deviceService) {
        this.deviceService = deviceService;
    }

    @GetMapping()
    public ResponseEntity<List<DeviceDTO>> getDevices() {
        List<DeviceDTO> dtos = deviceService.findDevices();
        for (DeviceDTO dto : dtos) {
            Link personLink = linkTo(methodOn(DeviceController.class)
                    .getDevice(dto.getId())).withRel("personDetails");
            dto.add(personLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping(value = "/{id}")
    public ResponseEntity<Long> insertProsumer(@Valid @RequestBody DeviceDTO deviceDto, @PathVariable("id") Long UserId) {
        Long deviceId = deviceService.insert(deviceDto, UserId);
        return new ResponseEntity<>(deviceId, HttpStatus.CREATED);

    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<DeviceDTO> getDevice(@PathVariable("id") Long deviceId) {
        DeviceDTO dto = deviceService.findDeviceById(deviceId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }


    @PutMapping(value = "/updateDevice/{id}/{sensId}")
    DeviceDTO replaceEmployee(@RequestBody DeviceDTO newEmployee, @PathVariable("id") Long  id,@PathVariable("id") Long  sensId) {
        return deviceService.update(newEmployee, id, sensId);
    }


    @DeleteMapping(value = "deleteDevice/{id}")
    ResponseEntity<Long>  deleteById(@PathVariable("id") Long  id){
        deviceService.delete(id);
        return new ResponseEntity<>(id, HttpStatus.CREATED);
    }

    @GetMapping(value = "/client/{id}")
    public  ResponseEntity<List<DeviceDTO>>checkDevice(@PathVariable("id") Long deviceId) {
        List<DeviceDTO> dtos  = deviceService.checkDevice(deviceId);

        for (DeviceDTO dto : dtos) {
            Link personLink = linkTo(methodOn(DeviceController.class)
                    .getDevice(dto.getId())).withRel("personDetails");
            dto.add(personLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);

    }

}
