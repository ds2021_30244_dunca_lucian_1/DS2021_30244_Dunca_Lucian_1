package ro.tuc.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.SensorDTO;
import ro.tuc.ds2020.dtos.SensorDataDTO;
import ro.tuc.ds2020.services.SensorDataService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/sensordata")
public class SensorDataController {

    private final SensorDataService sensorDataService;

    @Autowired
    public SensorDataController(SensorDataService sensorDataService) {
        this.sensorDataService = sensorDataService;
    }

    @GetMapping()
    public ResponseEntity<List<SensorDataDTO>> getDevices() {
        List<SensorDataDTO> dtos = sensorDataService.findDevices();
        for (SensorDataDTO dto : dtos) {
            Link personLink = linkTo(methodOn(DeviceController.class)
                    .getDevice(dto.getId())).withRel("personDetails");
            dto.add(personLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<Long> insertPro(@Valid @RequestBody SensorDataDTO sensorDTO) {
        Long deviceId = sensorDataService.insert2(sensorDTO);
        return new ResponseEntity<>(deviceId, HttpStatus.CREATED);

    }
    @PostMapping(value = "/{id}")
    public ResponseEntity<Long> insertProsumer(@Valid @RequestBody SensorDataDTO sensorDTO,@PathVariable("id") Long sensorId) {
        Long deviceId = sensorDataService.insert(sensorDTO, sensorId);
        return new ResponseEntity<>(deviceId, HttpStatus.CREATED);

    }



    @DeleteMapping(value = "deleteSensorData/{id}")
    void deleteById(@PathVariable("id") Long  id){
        sensorDataService.delete(id);
    }


    @GetMapping(value = "/{id}")
    public ResponseEntity<List<SensorDataDTO>> checkDeviceId(@PathVariable("id") Long sensorId) {
        List<SensorDataDTO> dtos = sensorDataService.findDevices();
        for (SensorDataDTO dto : dtos) {
            Link personLink = linkTo(methodOn(DeviceController.class)
                    .getDevice(dto.getId())).withRel("personDetails");
            dto.add(personLink);
        }

        List<SensorDataDTO> filteredDtos = new ArrayList<SensorDataDTO>();
        for (SensorDataDTO dto : dtos) {
            if(dto.getSensor() !=null)
                if(dto.getSensor().getId() == sensorId)
                    filteredDtos.add(dto);
        }
         //dtos = dtos.stream().filter(s-> s.getSensor().getId() == sensorId) .collect(Collectors.toList());;

        return new ResponseEntity<>(filteredDtos, HttpStatus.OK);


    }
}
