package ro.tuc.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.dtos.PersonDetailsDTO;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.services.PersonService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/person")
public class PersonController {

    private final PersonService personService;

    @Autowired
    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping()
    public ResponseEntity<List<PersonDTO>> getPersons() {
        List<PersonDTO> dtos = personService.findPersons();
        for (PersonDTO dto : dtos) {
            Link personLink = linkTo(methodOn(PersonController.class)
                    .getPerson(dto.getId())).withRel("personDetails");
            dto.add(personLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }
//, @PathVariable("id") Long deviceId
    @PostMapping
    public ResponseEntity<Long> insertProsumer(@Valid @RequestBody PersonDetailsDTO personDTO) {
        Long personID = personService.insert(personDTO);
        return new ResponseEntity<>(personID, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<PersonDetailsDTO> getPerson(@PathVariable("id") Long personId) {
        PersonDetailsDTO dto = personService.findPersonById(personId);
        //System.out.println(dto.getDevice().get(0).getDescription());
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    //TODO: UPDATE, DELETE per resource

    @PutMapping(value = "/updatePerson/{id}")
    PersonDetailsDTO replaceEmployee(@RequestBody PersonDetailsDTO newEmployee, @PathVariable("id") Long  id) {
        return personService.update(newEmployee, id);
    }

    @PutMapping(value = "/addDevice/{idPerson}/{idDevice}")
    public ResponseEntity<Long> addPerson(@PathVariable("idPerson") Long  idPerson,@PathVariable("idDevice") Long  idDevice) {
        personService.addDevice(idPerson, idDevice);
        return new ResponseEntity<>(idPerson, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "deletePerson/{id}")
    public ResponseEntity<Long> deleteById(@PathVariable("id") Long  id){
        personService.delete(id);
        return new ResponseEntity<>(id, HttpStatus.CREATED);
    }


    @ResponseBody
    @PostMapping(value = "/login")
    public Person checkUser(@RequestBody PersonDetailsDTO person) {
        return personService.checkPerson(person);
    }

}
