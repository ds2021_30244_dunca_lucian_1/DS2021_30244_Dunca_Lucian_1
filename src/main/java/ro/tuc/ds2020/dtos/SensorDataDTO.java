package ro.tuc.ds2020.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.RepresentationModel;
import ro.tuc.ds2020.entities.Sensor;

import javax.persistence.Column;
import java.sql.Timestamp;
import java.util.Objects;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SensorDataDTO extends RepresentationModel<SensorDataDTO> {
    private Long id;
    private Timestamp time;
    private float energyCons;
    private Sensor sensor;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SensorDataDTO sensorDTO = (SensorDataDTO) o;
        return time.equals(sensorDTO.time)  &&  energyCons == sensorDTO.energyCons;

    }

    @Override
    public int hashCode() {
        return Objects.hash(time, energyCons);
    }

}
