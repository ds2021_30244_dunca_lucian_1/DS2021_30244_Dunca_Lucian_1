package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;
import ro.tuc.ds2020.dtos.validators.annotation.AgeLimit;
import ro.tuc.ds2020.entities.SmartDevice;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class PersonDetailsDTO extends RepresentationModel<PersonDetailsDTO> {

    private Long id;
    @NotNull
    private String name;
    @NotNull

    private String password;

    private String address;
    @AgeLimit(limit = 18)
    private int age;
    private String role;

    private List<SmartDevice> device  =new ArrayList<SmartDevice>();

    public PersonDetailsDTO() {
    }

    public PersonDetailsDTO(Long id, @NotNull String name, @NotNull String password, String address, int age, String role, List<SmartDevice> device) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.address = address;
        this.age = age;
        this.role = role;
        this.device = device;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public PersonDetailsDTO(Long id, @NotNull String name, @NotNull String address, int age) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.age = age;
    }

    public PersonDetailsDTO(@NotNull String name, @NotNull String address, int age, String role) {
        this.name = name;
        this.address = address;
        this.age = age;
        this.role = role;
    }


    public List<SmartDevice> getDevice() {
        return device;
    }

    public void setDevice(List<SmartDevice> device) {
        this.device = device;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
    public void addDevice(SmartDevice device) {
        this.device.add(device);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonDetailsDTO that = (PersonDetailsDTO) o;
        return age == that.age &&
                Objects.equals(name, that.name) &&
                Objects.equals(address, that.address) && Objects.equals(role, that.role);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, address, age, role);
    }
}
