package ro.tuc.ds2020.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.RepresentationModel;

import java.util.Objects;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SensorDTO extends RepresentationModel<SensorDTO> {
    private Long id;
    private String description;
    private float maxValue;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SensorDTO sensorDTO = (SensorDTO) o;
        return maxValue == sensorDTO.maxValue &&
                Objects.equals(description, sensorDTO.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(description, maxValue);
    }

}
