package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.SensorDTO;
import ro.tuc.ds2020.dtos.SensorDataDTO;
import ro.tuc.ds2020.entities.Sensor;
import ro.tuc.ds2020.entities.SensorData;

public class SensorDataBuilder {

    private SensorDataBuilder() {
    }

    public static SensorDataDTO toSensorDTO(SensorData device) {
        return new SensorDataDTO(device.getId(),device.getTime(),  device.getEnergyCons(), device.getSensor());
    }

    public static SensorData toEntity(SensorDataDTO device) {
        return new SensorData(device.getId(),device.getTime(),  device.getEnergyCons(), device.getSensor());
    }
}
