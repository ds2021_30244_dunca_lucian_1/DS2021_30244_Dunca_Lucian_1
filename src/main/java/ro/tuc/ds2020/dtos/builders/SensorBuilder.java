package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.dtos.PersonDetailsDTO;
import ro.tuc.ds2020.dtos.SensorDTO;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.entities.Sensor;
import ro.tuc.ds2020.entities.SmartDevice;

public class SensorBuilder {
    private SensorBuilder() {
    }

    public static SensorDTO toSensorDTO(Sensor device) {
        return new SensorDTO(device.getId(),device.getDescription(),  device.getMaxValue());
    }

    public static Sensor toEntity(SensorDTO device) {
        return new Sensor(device.getId(),device.getDescription(),  device.getMaxValue());
    }

}
