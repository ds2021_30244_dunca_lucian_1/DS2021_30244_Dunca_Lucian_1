package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.dtos.PersonDetailsDTO;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.entities.SmartDevice;

public class DeviceBuilder {
    private DeviceBuilder() {
    }

    public static DeviceDTO toDeviceDTO(SmartDevice device) {
        return new DeviceDTO(device.getId(),device.getDescription(),  device.getAddress(),device.getMaxEnergyCons(),device.getAvgEnergyCons(), device.getSensor().getId(), device.getPersonId());
    }

    public static SmartDevice toEntity(DeviceDTO device) {
        return new SmartDevice(device.getDescription(),  device.getAddress(),device.getMaxEnergyCons(),device.getAvgEnergyCons());
    }

}
