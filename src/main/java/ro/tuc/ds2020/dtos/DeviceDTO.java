package ro.tuc.ds2020.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.RepresentationModel;
import ro.tuc.ds2020.entities.Sensor;

import java.util.Objects;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DeviceDTO extends RepresentationModel<DeviceDTO> {
    private Long id;
    private String description;
    private String address;
    private float maxEnergyCons;
    private int avgEnergyCons;
    private Long sensorId;
    private Long person_id;

    public Long getPerson_id() {
        return person_id;
    }

    public void setPerson_id(Long person_id) {
        this.person_id = person_id;
    }

    public DeviceDTO(String description, String address, float maxEnergyCons, int avgEnergyCons, Long sensorId, Long person_id) {
        this.description = description;
        this.address = address;
        this.maxEnergyCons = maxEnergyCons;
        this.avgEnergyCons = avgEnergyCons;
        this.sensorId = sensorId;
        this.person_id = person_id;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeviceDTO deviceDTO = (DeviceDTO) o;
        return maxEnergyCons == deviceDTO.maxEnergyCons && avgEnergyCons == deviceDTO.avgEnergyCons &&
                Objects.equals(description, deviceDTO.description) &&
                        Objects.equals(address, deviceDTO.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(description, address,maxEnergyCons, avgEnergyCons);
    }

}
