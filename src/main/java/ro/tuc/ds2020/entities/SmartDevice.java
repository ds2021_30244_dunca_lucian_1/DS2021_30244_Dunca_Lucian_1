package ro.tuc.ds2020.entities;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Entity
public class SmartDevice implements Serializable {

    private static final long serialVersionUID = 1L;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "maxEnergyCons", nullable = false)
    private float maxEnergyCons;

    @Column(name = "avgEnergyCons", nullable = false)
    private int avgEnergyCons;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "sensorId")
    private Sensor sensor;

    @Column(name="personId")
    private Long personId;

    public SmartDevice() {

    }


    public SmartDevice(Long id, String description, String address, float maxEnergyCons, int avgEnergyCons, Sensor sensor, Long personId) {
        this.id = id;
        this.description = description;
        this.address = address;
        this.maxEnergyCons = maxEnergyCons;
        this.avgEnergyCons = avgEnergyCons;
        this.sensor = sensor;
        this.personId = personId;
    }

    public SmartDevice(Long deviceId) {
        this.id=deviceId;
    }

    public SmartDevice(String description, String address, float maxEnergyCons, int avgEnergyCons) {    
        this.description = description;
        this.address = address;
        this.maxEnergyCons = maxEnergyCons;
        this.avgEnergyCons = avgEnergyCons;
    }

    public Long getId() {
        return id;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public float getMaxEnergyCons() {
        return maxEnergyCons;
    }

    public void setMaxEnergyCons(float maxEnergyCons) {
        this.maxEnergyCons = maxEnergyCons;
    }

    public int getAvgEnergyCons() {
        return avgEnergyCons;
    }

    public void setAvgEnergyCons(int avgEnergyCons) {
        this.avgEnergyCons = avgEnergyCons;
    }

    public Sensor getSensor() {
        return sensor;
    }

    public void setSensor(Sensor sensor) {
        this.sensor = sensor;
    }
}
