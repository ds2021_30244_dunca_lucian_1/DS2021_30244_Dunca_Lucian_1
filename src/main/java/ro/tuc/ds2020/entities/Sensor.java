package ro.tuc.ds2020.entities;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
public class Sensor implements Serializable {

    private static final long serialVersionUID = 1L;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "maxValue", nullable = false)
    private float maxValue;


    @OneToMany( cascade = CascadeType.ALL ,fetch = FetchType.EAGER)
    @JoinColumn(name = "sensorId")
    private List<SensorData> sensorDataList  =new ArrayList<SensorData>();

    public Sensor() {

    }

    public Sensor(Long id, String description, float maxValue) {
        this.id = id;
        this.description = description;
        this.maxValue = maxValue;
    }

    public Sensor(Long id) {
        this.id = id;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(float maxValue) {
        this.maxValue = maxValue;
    }
}
