package ro.tuc.ds2020.entities;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.UUID;

@Entity
public class SensorData implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "time", nullable = false)
    private Timestamp time;

    @Column(name = "energyConsumption", nullable = false)
    private float energyCons;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name=  "sensorId")
    private Sensor sensor;



    public SensorData() {

    }

    public SensorData(Long id, Timestamp time, float energyCons,   Sensor sensor){
        this.id = id;
        this.time = time;
        this.energyCons = energyCons;
        this.sensor = sensor;
    }


    public SensorData(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Sensor getSensor() {
        return sensor;
    }

    public void setSensor(Sensor sensor) {
        this.sensor = sensor;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    public float getEnergyCons() {
        return energyCons;
    }

    public void setEnergyCons(float energyCons) {
        this.energyCons = energyCons;
    }
}
